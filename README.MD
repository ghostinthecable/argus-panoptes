# Argus Panoptes [Overwatch]
v1.271019

Overwatch, Panoptes' predecessor was designed primarily to scan every resolvable IPv4 address (3,706,452,992 in total).
Now, we have escalated the project to include screenshots of the scanned devices (port 80).
For research purposes only, we are releasing this 'miner' program for users to aid us in our quest to get as many screenshots as possible.

---

## Dependencies

To use Panoptes (v1.271019), the user must be running a distribution of Linux, with the following dependencies installed:
- **rsync** (https://rsync.samba.org/)
To install:
```
https://download.samba.org/pub/rsync/
or
apt-get install rsync
```

- **Python**
```
sudo apt-get install python -y
```

- **phantomjs** (https://phantomjs.org/)
To install:
```
sudo apt-get install build-essential chrpath libssl-dev libxft-dev libfreetype6-dev libfreetype6 libfontconfig1-dev libfontconfig1 -y

sudo wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2

sudo ln -s /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/

sudo apt-get install phantomjs -y
```

---

## Usage
Visit our public github repo:
https://github.com/ghostinthecable/Argus-Panoptes

The first run is going to be comprised of 4 "groups", each group can contain up to 5 "miners" (members), as within each branch are 5 scripts (comprised of the bash commands necessary to render the port, alongside the phantomjs renderer).

To get a the list of files:
```
cd ~/yourdir/ ; git clone https://github.com/ghostinthecable/Argus-Panoptes
```

Once you have gotten the first public batch, you should see 6 directories with the files inside:
![cloned repo](http://panoptes.is-a-geek.org/panoptes/img/clonedrepo.png)
![cloned repo](http://panoptes.is-a-geek.org/panoptes/img/treeview.png)

--- 

## Procedure
1. - cd ~/yourdir/ ; git clone https://github.com/ghostinthecable/Argus-Panoptes
2. - Assign the permissions: 
```
chmod 755 $(find $filepath -type f -name '[abcdef]*')
```
3. - (For speedz, don't apply a password on the key) Configure and copy the id over to our infrastructure:
```
ssh-keygen -t rsa -b 4096
ssh-copy-id argus@panoptes.is-a-geek.org
```  

Community password is: 
```
4Rgu5P4n0Pt35p455  
```

*Note: 
```
This system is for the use of authorized users only.             
Individuals using this computer system without authority, or in   
excess of their authority, are subject to having all of their     
activities on this system monitored and recorded by system        
personnel.                                                        
                                                                  
In the course of monitoring individuals improperly using this     
system, or in the course of system maintenance, the activities    
of authorized users may also be monitored.                        
                                                                  
Anyone using this system expressly consents to such monitoring    
and is advised that if such monitoring reveals possible           
evidence of criminal activity, system personnel may provide the   
evidence of such monitoring to law enforcement officials.         
```  

4. - Inside the folder '~/yourdir/Argus-Panoptes/001/', you will see a file called 'test', execute it like a bash script: ./test
If the rsync proves successful, you are now ready to start! \o/
If it didn't, i'm sorry go check the configs.

![test](http://panoptes.is-a-geek.org/panoptes/img/test.JPG)

---

## Benchmark Averages
Our results (VPS-01):
```
HOST: VPS-01
---------------------------------------------------			
5 sec = Start at: 14:35:34					  				
		Finish at: 14:34:39					  				
		Total Screenshots taken: 6							
		Total avg. Screenshots / Instance: 0.15 			
		Total Runtime: 5 Sec.				  				
		Total Instances: 39					  				
		Total avg. Screenshot / Sec: 1.2 					
		Total avg. Screenshot / 5 Sec: 6	  				
---------------------------------------------------			
1min = Start at: 14:27:01					  				
		Finish at: 14:28:58					              
		Total Screenshots taken: 109						
		Total avg. Screenshots / Instance: 2.79				
		Total Runtime: 57 Sec.				              
		Total Instances: 39					              
		Total avg. Screenshot / Sec: 1.91                 
		Total avg. Screenshot / 5 Sec: 5.45               
---------------------------------------------------       
5 min = Start at: 14:36:58 - 1955 files                   
		Finish at: 14:42:01 - 2567 files                  
		Total Screenshots taken: 612                      
		Total avg. Screenshots / Instance: 15.69			
		Total Runtime: 303 Sec.                           
		Total Instances: 39                               
		Total avg. Screenshot / Sec: 2.01                 
		Total avg. Screenshot / 5 Sec: 10.5               
---------------------------------------------------       
15 min = Start at: 14:42:01 - 1955 files					
		Finish at: 14:57:00 - 4159 files                  
		Total Screenshots taken: 2204                     
		Total avg. Screenshots / Instance: 56.51			
		Total Runtime: 899 Sec.                           
		Total Instances: 39                               
		Total avg. Screenshot / Sec: 2.45                 
		Total avg. Screenshot / 5 Sec: 12.25              
---------------------------------------------------       
30 min = Start at: 14:57:00 - 4159 files					
		Finish at: 15:26:58 - 7369 files                  
		Total Screenshots taken: 3120                     
		Total avg. Screenshots / Instance: 80 				
		Total Runtime: 1798 Sec.                          
		Total Instances: 39                               
		Total avg. Screenshot / Sec: 1.78                 
		Total avg. Screenshot / 5 Sec: 8.92               
---------------------------------------------------       
1 hr =  Start at: 15:26:58 - 7369 files						
		Finish at: 16:26:57 - 13876 files                 
		Total Screenshots taken: 6,507                    
		Total avg. Screenshots / Instance: 166.84			
		Total Runtime: 3599 Sec.                          
		Total Instances: 39                               
		Total avg. Screenshot / Sec: 1.80                 
		Total avg. Screenshot / 5 Sec: 9.04               
----------------------------------------------------------

```
Our results (VPS-02):
```
HOST: VPS-02
---------------------------------------------------
5 sec = Start at: 17:21:30 - 3289 files
		Finish at: 17:21:35 - 3294 files
		Total Screenshots taken: 5
		Total avg. Screenshots / Instance: 0.17
		Total Runtime: 5 Sec.
		Total Instances: 28
		Total avg. Screenshot / Sec: 1
		Total avg. Screenshot / 5 Sec: 5
---------------------------------------------------
1 min = Start at: 17:26:49 - 3762 files
		Finish at: 17:27:49 - 3846 files
		Total Screenshots taken: 84
		Total avg. Screenshots / Instance: 3
		Total Runtime: 60 Sec.
		Total Instances: 28
		Total avg. Screenshot / Sec: 1.4
		Total avg. Screenshot / 5 Sec: 7		  
---------------------------------------------------
5 min = Start at: 17:27:49 - 3846 files
		Finish at: 17:32:47 - 4271 files
		Total Screenshots taken: 425
		Total avg. Screenshots / Instance: 15.17
		Total Runtime: 298 Sec.
		Total Instances: 28
		Total avg. Screenshot / Sec: 1.42
		Total avg. Screenshot / 5 Sec: 7.13	  
---------------------------------------------------
15 min = Start at: 17:32:47 - 4271 files
		Finish at: 17:47:47 - 5736 files
		Total Screenshots taken: 1465
		Total avg. Screenshots / Instance: 52.32
		Total Runtime: 900 Sec.
		Total Instances: 28 
		Total avg. Screenshot / Sec: 1.62
		Total avg. Screenshot / 5 Sec: 8.13	  
---------------------------------------------------
30 min = Start at: 17:47:47 - 5736 files
		Finish at: 18:17:48 - 8907 files
		Total Screenshots taken: 3171
		Total avg. Screenshots / Instance: 113.25‬
		Total Runtime: 1801 Sec.
		Total Instances: 28 
		Total avg. Screenshot / Sec: 1.76
		Total avg. Screenshot / 5 Sec: 8.80	  
---------------------------------------------------
1h =    Start at: 18:17:48 - 8907 files
   		Finish at: 19:17:45 - 14667 files
   		Total Screenshots taken: 5760‬
		Total avg. Screenshots / Instance: 205.71
   		Total Runtime: 6597 Sec.
		Total Instances: 28
   		Total avg. Screenshot / Sec: 0.87
   		Total avg. Screenshot / 5 Sec: 	4.35  
---------------------------------------------------

```

Our benchmarking total (2 instances):
```
The average is 6133.5 per hour
```

---

## Organization Policy
In all honesty, we're not expecting many participants, however those who do, we would encourage you to go to this project's site:
+ http://panoptes.is-a-geek.org/panoptes/ to view the current jobs in progress.
+ http://panoptes.is-a-geek.org/panoptes/put.php to add a chunk you're working on.

---

## Telegram Group
You may join our Telegram group for updates and other stuffs at: [tbc]

---

## With Thanks To:
- Mr. S. -> Thank you for your patience with me
- Twitter community for your shell scripting wisdom
